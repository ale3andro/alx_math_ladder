/** FROM https://www.kevinleary.net/javascript-get-url-parameters/
 * JavaScript Get URL Parameter
 * 
 * @param String prop The specific URL parameter you want to retreive the value for
 * @return String|Object If prop is provided a string value is returned, otherwise an object of all properties is returned
 */
function getUrlParams( prop ) {
  var params = {};
  var search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) );
  var definitions = search.split( '&' );

  definitions.forEach( function( val, key ) {
      var parts = val.split( '=', 2 );
      params[ parts[ 0 ] ] = parts[ 1 ];
  } );

  return ( prop && prop in params ) ? params[ prop ] : params;
}

var number_of_moves = 0;
var move=0;
var game_data;
var game_player;
var game_stage;
var game_layer;
var first_run=true;

$( document ).ready(function() {

  $('a.fancybox').fancybox({
    'modal': true,
    'type': 'iframe',
    'showCloseButton' : true,
    afterShow : function() {
      $('.fancybox-skin').append('<a title="Close" class="fancybox-item fancybox-close" href="#" onclick="javascript:check_move();"></a>');
      
    }
  });

  var game_id = getUrlParams('id');
  if ( (game_id.length>0) && (game_id!='not available') ) {
      var jqxhr = $.getJSON("games/" + game_id + ".json", function(data) {
        game_data = data;
        console.log('game loaded');
        data['player']['route'].forEach(function(object) {
          number_of_moves++;
        });
      
        // first we need to create a stage
        var stage = new Konva.Stage({
          container: 'container',   // id of container <div>
          width: 963,
          height: 686
        });
        game_stage = stage;

        // then create layer
        var layer0 = new Konva.Layer();
        var layer1 = new Konva.Layer();
        mapObj = new Image();
        mapObj.src = data['map']['image_url'];
        mapObj.onload = function() {
          map = new Konva.Image({
            x: 0,
            y: 0,
            image: mapObj,
            width: data['map']['image_width'],
            height: data['map']['image_height']
          });
          // add the shape to the layer
          layer0.add(map);
          stage.add(layer0);
        };

        playerObj = new Image();
        playerObj.src = data['player']['image_url'];
        playerObj.onload = function() {
          player = new Konva.Image({
            x: data['player']['route'][0]['x'],
            y: data['player']['route'][0]['y'],
            image: playerObj,
            width: data['player']['image_width'],
            height: data['player']['image_height']
          });
          game_player=player;
          // add the shape to the layer
          layer1.add(player);
          stage.add(layer1);
        };

        prizeObj = new Image();
        prizeObj.src = data['prize']['image_url'];
        prizeObj.onload = function() {
          prize = new Konva.Image({
            x: data['prize']['x'],
            y: data['prize']['y'],
            image: prizeObj,
            width: data['prize']['image_width'],
            height: data['prize']['image_height']
          });
          // add the shape to the layer
          layer1.add(prize);
          stage.add(layer1);
        };
        game_layer=layer1;
        
        $("#alx_link").prop("href", data['player']['route'][0]['challenge_url'] + "&questId=quest0");
      })
      .fail(function() {
          console.log('failed to load the game');
          
      });
  }
  else {
      alert("Δεν ορίστηκε το id της δραστηριότητας");
  }      
});

function move_player(x, y) {
  game_player.setX(x);
  game_player.setY(y);
  game_layer.moveToTop();
  game_layer.draw();
}

function check_move() {
  jQuery.fancybox.close()
  if (sessionStorage.getItem('quest0')!=1) {
    console.log('Game Lost');
    return false;
  } else {
    console.log('Game Won');
    if ((move+1)<number_of_moves) {
      move++;
      move_player(game_data['player']['route'][move]['x'], game_data['player']['route'][move]['y']);
      $("#alx_link").prop("href", game_data['player']['route'][move]['challenge_url']);
    } else {
      alert('game over');
    }
    return true;
  }
}

function setup_game(data) {
  // first we need to create a stage
  var stage = new Konva.Stage({
    container: 'container',   // id of container <div>
    width: 963,
    height: 686
  });
  game_stage = stage;

  // then create layer
  var layer0 = new Konva.Layer();
  var layer1 = new Konva.Layer();
  mapObj = new Image();
  mapObj.src = data['map']['image_url'];
  mapObj.onload = function() {
      map = new Konva.Image({
          x: 0,
          y: 0,
          image: mapObj,
          width: data['map']['image_width'],
          height: data['map']['image_height']
      });
    // add the shape to the layer
    layer0.add(map);
    stage.add(layer0);
  };

  playerObj = new Image();
  playerObj.src = data['player']['image_url'];
  playerObj.onload = function() {
      player = new Konva.Image({
          x: data['player']['route'][0]['x'],
          y: data['player']['route'][0]['y'],
          image: playerObj,
          width: data['player']['image_width'],
          height: data['player']['image_height']
      });
      game_player=player;
    // add the shape to the layer
    layer1.add(player);
    stage.add(layer1);
  };

  prizeObj = new Image();
  prizeObj.src = data['prize']['image_url'];
  prizeObj.onload = function() {
      prize = new Konva.Image({
          x: data['prize']['x'],
          y: data['prize']['y'],
          image: prizeObj,
          width: data['prize']['image_width'],
          height: data['prize']['image_height']
      });

    // add the shape to the layer
    layer1.add(prize);
    stage.add(layer1);
  };
  game_layer=layer1;

  // Game logic
  step_x = 0;
  step_y = 0;
  step_x_forward_offset = 136;
  step_y_forward_offset = 136;

  var prize_disappear = new Konva.Animation(function(frame) {
    if (prize.opacity()>0) {
      prize.opacity(prize.opacity()-0.5);
    }
  }, layer1);
  
  var anim_up = new Konva.Animation(function(frame) {
    if (player.x()<((step_x)*step_x_forward_offset)+30)
    {
        theX = player.x() + 2;
        theY = 7 + 135; //player.y();
        player.absolutePosition({
            x: theX,
            y: theY
        }); 
    } else {
      anim_up.stop();
      button_status(false);
    }
  }, layer1);
  
  var anim_down = new Konva.Animation(function(frame) {
    if (player.x()>((step_x)*step_x_forward_offset)+30)
    {
        theX = player.x() - 2;
        theY = player.y();
        player.absolutePosition({
            x: theX,
            y: theY
        });
    } else {
      anim_down.stop();
      button_status(false);
    }
  }, layer1);

  stage.draw();
}
